echo "fermo servizio alfresco"
/alfresco/alfresco.sh stop
echo "copio file configurazioni custom"
yes | cp -rf /customconfig/alfresco-global.properties /alfresco/tomcat/shared/classes/
chmod 640 /alfresco/tomcat/shared/classes/alfresco-global.properties
chown root:root /alfresco/tomcat/shared/classes/alfresco-global.properties
echo "copio file custom workflow"
cp /customconfig/kb-approval-process-definition.xml /alfresco/tomcat/shared/classes/alfresco/extension/
cp /customconfig/kb-workflow-context.xml /alfresco/tomcat/shared/classes/alfresco/extension/
echo "riavvio servizio alfresco"
/alfresco/alfresco.sh start
